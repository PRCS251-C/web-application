<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sign in - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
html {
    overflow-y: scroll;
}
input{
    width: 60%;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body>
        <form id="SignInForm" name="SignInForm" action="SignInServlet" method="post" onkeypress="return event.keyCode != 13;">
<%           
    
%>
        <img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png">               
                
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';">Checkout</td>            
            </table>
    <center>

        <p class="biggerTitle" style="color: white">Sign in to your Pizzariño Account</p>
        <p class="subtitle" style="color: white">Don't have an account? Sign up <a href='CreateAccount.jsp'>here</a>.</p>
        </br>
        </br>
        <div style="width: 30%;">                   
                        
                        <span class="subtitle" style="float: left">Email Address: </span>
                            <input type="text" id="signInEmail" name="signInEmail" style="width:60%; float: right" required></br></br>
                        <span class="subtitle" style="float: left">Password: </span>
                            <input type="password" id="signInPW" name="signInPW" style="width: 60%; float: right" required></br></br> 
        </div>
                        
                <button type="button" onclick="window.history.back()" class="genericBack">Back</button>
                <button type="button" id="btnSignUp" onclick="signInCheck()" class="genericForward">Sign in</button>
                <p id="signInCheckP" class="title red" style="display: none;">INVALID LOGIN DETAILS</p>
    </center>    
    
    <script type="text/javascript">
        function signInCheck(){
            var testEmail = "user@user.com";
            var testPW = "test";
            var inputEmail = signInEmail.value;
            var inputPW = signInPW.value;
            alert("test email = " + testEmail);
            alert("test pw = " + testPW);
            alert("user email = " + inputEmail);
            alert("user pw = " + inputPW);
            if((testEmail===inputEmail)&&(testPW===inputPW)){
                document.getElementById("SignInForm").submit();
            }else{
                signInCheckP.style.display = 'block';
            }
            
        }
       
    </script>
        </form>
    </body>
</html>
