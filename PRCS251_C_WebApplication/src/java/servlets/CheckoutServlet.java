/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;
 
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
/**
 *
 * @author ndrichardson1
 */
@WebServlet(name = "CheckoutServlet", urlPatterns = {"/CheckoutServlet"})
public class CheckoutServlet extends HttpServlet {
 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //getting the values for the cookies
        HttpSession session = request.getSession();
        String firstNameCO = request.getParameter("checkoutFirstName");
        String lastNameCO = request.getParameter("checkoutLastName");
        String emailCO = request.getParameter("checkoutEmail");
        String phoneNoCO = request.getParameter("checkoutPhNo");
        String address1CO = request.getParameter("checkoutAddress1");
        String address2CO = request.getParameter("checkoutAddress2");
        String cityCO = request.getParameter("checkoutCity");
        String checked = "";
       
        if(request.getParameter("useDeliveryAddress") == null){
            checked = "false";            
        }else{
            checked = "true";
        }
         session.setAttribute("CheckedBox", checked);
        //setting the cookies
        Cookie FirstNameCookie = new Cookie("FirstName", firstNameCO);
        Cookie LastNameCookie = new Cookie("LastName", lastNameCO);
        Cookie EmailCookie = new Cookie("Email", emailCO);
        Cookie PhoneNoCookie = new Cookie("PhoneNumber", phoneNoCO);
        Cookie Address1Cookie = new Cookie("AddressLine1", address1CO);
        Cookie Address2Cookie = new Cookie("AddressLine2", address2CO);
        Cookie CityCookie = new Cookie("City", cityCO);
        //setting the life of the cookies
        FirstNameCookie.setMaxAge(-1);
        LastNameCookie.setMaxAge(-1);
        EmailCookie.setMaxAge(-1);
        PhoneNoCookie.setMaxAge(-1);
        Address1Cookie.setMaxAge(-1);
        Address2Cookie.setMaxAge(-1);
        CityCookie.setMaxAge(-1);
        //adding the cookies
        response.addCookie(FirstNameCookie);
        response.addCookie(LastNameCookie);
        response.addCookie(EmailCookie);
        response.addCookie(PhoneNoCookie);
        response.addCookie(Address1Cookie);
        response.addCookie(Address2Cookie);
        response.addCookie(CityCookie);    
       
       
       
        try (PrintWriter out = response.getWriter()) {
            response.sendRedirect("Payment.jsp");  
        }
    }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
 
}
