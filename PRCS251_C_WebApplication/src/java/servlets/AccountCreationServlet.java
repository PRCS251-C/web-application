/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;
 
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
/**
 *
 * @author ndrichardson1
 */
@WebServlet(name = "AccountCreationServlet", urlPatterns = {"/AccountCreationServlet"})
public class AccountCreationServlet extends HttpServlet {
 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String emailSignUp = request.getParameter("accEmail");
        String passwordSignUp = request.getParameter("accPW");
        String firstNameSignUp = request.getParameter("accFirstName");
        String lastNameSignUp = request.getParameter("accLastName");
        String phNoSignUp = request.getParameter("accPhNo");
        String address1SignUp = request.getParameter("accAddress1");
        String address2SignUp = request.getParameter("accAddress2");
        String citySignUp = request.getParameter("accCity");
        String postcodeSignUp = request.getParameter("accPostcode");
        Cookie emailUpCookie = new Cookie("SignUpEmail", emailSignUp);
        Cookie pwUpCookie = new Cookie("SignUpPW", passwordSignUp);
        Cookie firstNameUpCookie = new Cookie("SignUpFN", firstNameSignUp);
        Cookie lastNameUpCookie = new Cookie("SignUpFN", lastNameSignUp);
        Cookie phNoUpCookie = new Cookie("SignUpPH", phNoSignUp);
        Cookie addr1UpCookie = new Cookie("SignUpAddr1", address1SignUp);
        Cookie addr2UpCookie = new Cookie("SignUpAddr2", address2SignUp);
        Cookie cityUpCookie = new Cookie("SignUpCity", citySignUp);
        Cookie postUpCookie = new Cookie("SignUpPost", postcodeSignUp);
        emailUpCookie.setMaxAge(-1);
        pwUpCookie.setMaxAge(-1);
        firstNameUpCookie.setMaxAge(-1);
        lastNameUpCookie.setMaxAge(-1);
        phNoUpCookie.setMaxAge(-1);
        addr1UpCookie.setMaxAge(-1);
        addr2UpCookie.setMaxAge(-1);
        cityUpCookie.setMaxAge(-1);
        postUpCookie.setMaxAge(-1);
        response.addCookie(emailUpCookie);  
        response.addCookie(pwUpCookie);
        response.addCookie(firstNameUpCookie);
        response.addCookie(lastNameUpCookie);
        response.addCookie(phNoUpCookie);
        response.addCookie(addr1UpCookie);
        response.addCookie(addr2UpCookie);
        response.addCookie(cityUpCookie);
        response.addCookie(postUpCookie);
        session.setAttribute("loggedIn", "1");
        try (PrintWriter out = response.getWriter()) {
            out.println(emailSignUp);
            out.println(passwordSignUp);
            out.println(firstNameSignUp);
            out.println(lastNameSignUp);
            out.println(phNoSignUp);
            out.println(address1SignUp);
            out.println(address2SignUp);
            out.println(citySignUp);
            out.println(postcodeSignUp);
            response.sendRedirect("Menu.jsp");
        }
    }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
 
}