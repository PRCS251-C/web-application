<%@page import="api.APIConnection"%>
<%@page import="datamodel.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datamodel.ModelController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Menu - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <style type="text/css">
html {
    overflow-y: auto;
}

        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css?id=2">
        
    </head>

    <body onload="loginCheck()">
         <form name="MenuForm" method="post" action="MenuServlet" onkeypress="return event.keyCode != 13;">
<%           
   Cookie cookie = null;
   Cookie[] cookies = null;
   String postcode = "";
   cookies = request.getCookies();
   String userLoggedIn = "";
   String userEmail = "";
   if( cookies != null ){
      for (int i = 0; i < cookies.length; i++){
         cookie = cookies[i];
         if(cookie.getName().equals("Postcode")){
             postcode = cookie.getValue();
         }else if(cookie.getName().equals("loggedIn")){
                userLoggedIn = cookie.getValue();             
         }else if(cookie.getName().equals("SignInEmail")){
                userEmail = cookie.getValue();             
         }
      }
  }

   
   %>            
                <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                                <span>Delivering to: </br>
                                <%=postcode%></span></br>                   
                            <button type="button" class="genericButton1" align="right" id="postcodeChangeBtn" style="height: 30px; width: 250px; font-size: 20px;" onclick="location.href='ChangePostcode.jsp';">Change?</button>
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div id="signInNo" class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                            <div id="signInYes" class="title" style="color: white; padding-right:0px; display: none;">
                                Welcome <%=userEmail%> </br>
                                <button type="button" onclick="location.href='SignOutServlet';" class="genericButton2" style="height:50px; width: 125px">Sign Out</button>
                            </div>
                        </th>
                    </tr>
               </table>
   
       
        <div class="fixToTop">       
        <table style="width: inherit" id="navBarTable">
                <td class="navBar selected" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';" id="baskettable">Basket: 0</td>            
            </table>     
        
            <table style="width:60%;" align="center">
                <td class="navBarMenuSmall" onclick="location.href='Menu.jsp#pizzas';">Pizzas</td>
                <td class="navBarMenuSmall" onclick="location.href='Menu.jsp#sides';">Sides</td>
                <td class="navBarMenuSmall" onclick="location.href='Menu.jsp#desserts';">Desserts</td>
                <td class="navBarMenuSmall" onclick="location.href='Menu.jsp#drinks';">Drinks</td>
            </table>
        </div>
        <div class="stopJumpDiv"></div>
           
        </br>
        <center>
        <div style="width: 70%">
        <a class="anchor" id="pizzas"></a>
        <p class="biggerTitle fancy" style="color: white">Pizzas</p>
        <div class="cyop" onclick="cyoptest()">
            <div class="imgOverlay">
                <p>Fancy something a little different?</p>
                <p>Try creating your own pizza.</p>
            </div>
        </div>
        <div id="pizzasDiv">
<!--            <div class="pizzaTable" id="1">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/pepppizza.png"></div>
                <div class="menuName">Passion of the Crust</div>
                <select class="menuSizeSelect">
                    <option value="1" selected="selected">Small - £7.99</option>
                    <option value="2">Medium - £9.99</option>
                    <option value="3">Large - £11.99</option>
                </select>
                <div class="menuCustomise">Customize</div>
                <div class="menuAdd pizza" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>
            <div class="pizzaTable" id="2">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/pepppizza.png"></div>
                <div class="menuName">Wake</div>
                <select class="menuSizeSelect">
                    <option value="1" selected="selected">Small - £7.99</option>
                    <option value="2">Medium - £9.99</option>
                    <option value="3">Large - £11.99</option>
                </select>
                <div class="menuCustomise">Customize</div>
                <div class="menuAdd pizza" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>
            <div class="pizzaTable" id="3">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/pepppizza.png"></div>
                <div class="menuName">Me</div>
                <select class="menuSizeSelect">
                    <option>Small - £7.99</option>
                    <option>Medium - £9.99</option>
                    <option>Large - £11.99</option>
                </select>
                <div class="menuCustomise">Customize</div>
                <div class="menuAdd pizza" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>
            <div class="pizzaTable" id="4">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/pepppizza.png"></div>
                <div class="menuName">Up</div>
                <select class="menuSizeSelect">
                    <option>Small - £7.99</option>
                    <option>Medium - £9.99</option>
                    <option>Large - £11.99</option>
                </select>
                <div class="menuCustomise">Customize</div>
                <div class="menuAdd pizza" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>
            <div class="pizzaTable" id="5">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/pepppizza.png"></div>
                <div class="menuName">(Wake Me Up Inside)</div>
                <select class="menuSizeSelect">
                    <option>Small - £7.99</option>
                    <option>Medium - £9.99</option>
                    <option>Large - £11.99</option>
                </select>
                <div class="menuCustomise">Customize</div>
                <div class="menuAdd pizza" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>-->
        </div>
       
        <a class="anchor" id="sides"></a>
        <p class="biggerTitle fancy" style="color: white">Sides</p>
        <div id="sidesDiv">
            <div class="otherTable" id="6">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/cheesygarlicbread.png"></div>
                <div class="menuName">Cheesy Garlic Bread</div>
                <div class="menuPrice">£2.99</div>
                <input class="menuQuantity" type="number" value="1" min="1"></input>
                <div class="menuAdd other" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>
        </div>
       
        <a class="anchor" id="desserts"></a>
        <p class="biggerTitle fancy" style="color: white">Desserts</p>
        <div id="dessertsDiv" id="7">
            <div class="otherTable">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/icecream.png"></div>
                <div class="menuName">Ice Cream</div>
                <div class="menuPrice">£3.99</div>
                <input class="menuQuantity" type="number" value="1" min="1"></input>
                <div class="menuAdd other" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>
        </div>
        <a  class="anchor" id="drinks"></a>
        <p class="biggerTitle fancy" style="color: white">Drinks</p>
        <div id="drinksDiv">
            <div class="otherTable" id="8">
                <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/dietpepsi.png"></div>
                <div class="menuName">Diet Pepsi - 2L</div>
                <div class="menuPrice">£1.99</div>
                <input class="menuQuantity" type="number" value="1" min="1"></input>
                <div class="menuAdd other" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>
            </div>
        </div>
    </center>
        <p id="testPTag"></p>
    <button type="button" onclick="goToTop()" id="topButton" class="topButton">Go to top</button>
    <p id="isLogged" hidden><%=userLoggedIn%></p>
<script>
var fixmeTop = $('.fixToTop').offset().top;

$(window).scroll(function() {
    var currentScroll = $(window).scrollTop();
    if (currentScroll >= fixmeTop) {
        $('.fixToTop').css({
            position: 'fixed',
            top: '0',
        });
        $('.stopJumpDiv').css({
            height: '153px'
        });
    } else {
        $('.fixToTop').css({
            position: 'static'
        });
        $('.stopJumpDiv').css({
            height: '0px'
        });
    }
});

function cyoptest(){
    alert("test cyop");
}

function loginCheck() {
    var isLoggedIn = isLogged.innerHTML;
        if (isLoggedIn === "0") {
            signInNo.style.display = 'block';
            signInYes.style.display = 'none';
        } else if (isLoggedIn === "1") {
            signInNo.style.display = 'none';
            signInYes.style.display = 'block';
        }             
                    
}
window.onscroll = function() {scrollDown();};

function scrollDown() {
    if (document.body.scrollTop > 153 || document.documentElement.scrollTop > 153) {
        document.getElementById("topButton").style.display = "block";
    } else {
        document.getElementById("topButton").style.display = "none";
    }
}

function goToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

var interval = false;

$('input').focus(function () {
    var $this = $(this);
    
    interval = setInterval( function () {
        if (($this.val() < 1) && $this.val().length !== 0) {
            if ($this.val() < 1) {
                $this.val(1);
            }
        }
    }, 50);
});

$('input').blur(function () {
    if (interval !== false) {
        window.clearInterval(interval);
        interval = false;
    }
});

var products = new Array();
var anySizeId = "0";

<% 
    ModelController.getInstance().populateProductData();
    ArrayList<Product> productList = ModelController.getInstance().getProductList();
    Integer productListSize = productList.size();
    String anySizeId = "0";
    ModelController.getInstance().populateSizeOptionsData();
    for (SizeOptions size : ModelController.getInstance().getSizeOptionsList()) {
        if (size.getSizeName().equals("Any")) {
            anySizeId = size.getSizeId().toString();
            System.out.println(anySizeId);
            break;
        }
        System.out.println(size.getSizeName());
    }
    Object[] productArray = productList.toArray();
    String productJson = APIConnection.mapper.writeValueAsString(productArray);
    Integer count = 0;
%>
    
products = <%=productJson%>;
anySizeId = <%=anySizeId%>;

populatePizzas();
populateSides();
populateDesserts();
populateDrinks();

function populatePizzas() {
    var html = "";
    
    for (var i = 0; i < products.length; i++) {
        if (products[i].PRODUCT_TYPE === "Pizza") {
            var isSelectedSet = false;
            html = html + '<div class="pizzaTable" id="' + products[i].PRODUCT_ID + '">';
            html = html + '    <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/pepppizza.png"></div>';
            html = html + '    <div class="menuName">' + products[i].PRODUCT_NAME + '</div>';
            html = html + '    <select class="menuSizeSelect">';
            for (var j = 0; j < products[i].PRODUCT_SIZES.length; j++) {
                if (isSelectedSet) {
                    html = html + '<option value="' + products[i].PRODUCT_SIZES[j].SIZE_ID + '">' + products[i].PRODUCT_SIZES[j].sizeName + ' - £' + products[i].PRODUCT_SIZES[j].PRICE + '</option>';
                } else {
                    html = html + '<option value="' + products[i].PRODUCT_SIZES[j].SIZE_ID + '" selected="selected">' + products[i].PRODUCT_SIZES[j].sizeName + ' - £' + products[i].PRODUCT_SIZES[j].PRICE + '</option>';
                    isSelectedSet = true;
                }
            }
            html = html + '    </select>';
            html = html + '    <div class="menuCustomise">Customize</div>';
            html = html + '    <div class="menuAdd pizza" id="addToBasketBtn">Add to basket</div>';
            html = html + '</div>';
        }
    }
    document.getElementById("testPTag").innerHTML = "Hello there";
    document.getElementById("pizzasDiv").innerHTML = html;
}

function populateSides() {
    var html = "";
    var sizeId = anySizeId;
    
    for (var i = 0; i < products.length; i++) {
        if (products[i].PRODUCT_TYPE === "Side") {
            var price = 0.00;
            html = html + '<div class="otherTable" id="' + products[i].PRODUCT_ID + '">';
            html = html + '    <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/cheesygarlicbread.png"></div>';
            html = html + '    <div class="menuName">' + products[i].PRODUCT_NAME + '</div>';
            for (var j = 0; j < products[i].PRODUCT_SIZES.length; j++) {
                if (products[i].PRODUCT_SIZES[j].SIZE_ID === sizeId) {
                    price = products[i].PRODUCT_SIZES[j].PRICE;
                    sizeId = products[i].PRODUCT_SIZES[j].SIZE_ID;
                    break;
                }
            }
            html = html + '    <div class="menuPrice" id="' + sizeId + '">£' + price + '</div>';
            html = html + '    <input class="menuQuantity" type="number" value="1" min="1"></input>';
            html = html + '    <div class="menuAdd other" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>';
            html = html + '</div>';
        }
    }
    
    document.getElementById("sidesDiv").innerHTML = html;
}

function populateDesserts() {
    var html = "";
    var sizeId = anySizeId;
    
    for (var i = 0; i < products.length; i++) {
        if (products[i].PRODUCT_TYPE === "Dessert") {
            var price = 0.00;
            html = html + '<div class="otherTable" id="' + products[i].PRODUCT_ID + '">';
            html = html + '    <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/icecream.png"></div>';
            html = html + '    <div class="menuName">' + products[i].PRODUCT_NAME + '</div>';
            for (var j = 0; j < products[i].PRODUCT_SIZES.length; j++) {
                if (products[i].PRODUCT_SIZES[j].SIZE_ID === sizeId) {
                    price = products[i].PRODUCT_SIZES[j].PRICE;
                    break;
                }
            }
            html = html + '    <div class="menuPrice" id="' + sizeId + '">£' + price + '</div>';
            html = html + '    <input class="menuQuantity" type="number" value="1" min="1"></input>';
            html = html + '    <div class="menuAdd other" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>';
            html = html + '</div>';
        }
    }
    
    document.getElementById("dessertsDiv").innerHTML = html;
}

function populateDrinks() {
        var html = "";
    var price = 0.00;
    
    for (var i = 0; i < products.length; i++) {
        if (products[i].PRODUCT_TYPE === "Drink") {
            var isSelected = false;
            html = html + '<div class="otherTable" id="' + products[i].PRODUCT_ID + '">';
            html = html + '    <div class="menuImgWrapper"><img style="margin-top: 20px" src="resources/images/dietpepsi.png"></div>';
            html = html + '    <div class="menuName">' + products[i].PRODUCT_NAME + '</div>';
            for (var j = 0; j < products[i].PRODUCT_SIZES.length; j++) {
                if (isSelectedSet) {
                    html = html + '<option value="' + products[i].PRODUCT_SIZES[j].SIZE_ID + '">' + products[i].PRODUCT_SIZES[j].sizeName + ' - £' + products[i].PRODUCT_SIZES[j].PRICE + '</option>';
                } else {
                    html = html + '<option value="' + products[i].PRODUCT_SIZES[j].SIZE_ID + '" selected="selected">' + products[i].PRODUCT_SIZES[j].sizeName + ' - £' + products[i].PRODUCT_SIZES[j].PRICE + '</option>';
                    isSelectedSet = true;
                }
            }
            html = html + '    <div class="menuPrice">£' + price + '</div>';
            html = html + '    <input class="menuQuantity" type="number" value="1" min="1"></input>';
            html = html + '    <div class="menuAdd pizza" id="addToBasketBtn" onclick="testBasketAdd()">Add to basket</div>';
            html = html + '</div>';
        }
    }
    
    document.getElementById("drinksDiv").innerHTML = html;
}

</script>
    <script src="resources/basket.js?id=2" type="text/javascript"></script>
         </form>
</body>
</html>