<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Account - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
html {
    overflow-y: scroll;
}
input{
    width: 60%;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body>
        <form action="AccountCreationServlet" name="AccountCreateForm" method="post" onkeypress="return event.keyCode != 13;">
<%           
    
%>
        <img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png">               
                
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';">Checkout</td>            
            </table>
    <center>

        <p class="biggerTitle" style="color: white">Sign up for a Pizzariño Account</p>
        <p class="subtitle" style="color: white">Sign up with Pizzariño and get personalised rewards, emails about our offers and a faster checkout system.</p>
        <p class="subtitle" style="color: white">Already have an account? Sign in <a href='SignIn.jsp'>here</a>.</p>
        <div style="width: 30%">
                        </br>
                        <p class="subtitle" style="color: white">Account details:</p>
                        <span class="subtitle" style="float: left;">Email Address: </span>
                        <input type="text" name="accEmail" style="width:60%; float: right" required></br></br>
                        <span class="subtitle" style="float: left">Password: </span>
                            <input type="password" name="accPW" style="width:60%; float: right" required></br></br> 
                        <span class="subtitle" style="float: left">Re-enter Password: </span>
                            <input type="password" name="accPW1Check" style="width:60%; float: right" required></br></br></br>
                        <p class="subtitle" style="color: white">Personal Information:</p>
                        <span class="subtitle" style="float: left">First Name: </span>
                            <input type="text" name="accFirstName" style="width:60%; float: right" required></br></br>
                        <span class="subtitle" style="float: left">Last Name: </span>
                            <input type="text" name="accLastName" style="width:60%; float: right" required></br></br>                        
                        <span class="subtitle" style="float: left">Phone Number: </span>
                            <input type="text" name="accPhNo" style="width:60%; float: right" required></br></br>
                        <span class="subtitle" style="float: left">Address Line 1: </span>
                            <input type="text" name="accAddress1"style="width:60%; float: right" required></br></br>
                        <span class="subtitle" style="float: left">Address Line 2: </span>
                            <input type="text" name="accAddress2" style="width:60%; float: right" required></br></br>
                        <span class="subtitle" style="float: left">City: </span>
                            <input type="text" name="accCity" style="width:60%; float: right" required></br></br>
                        <span class="subtitle" style="float: left">Postcode: </span>
                            <input type="text" name="accPostcode" style="width:60%; float: right" required></br></br>
        </div>
                        
                <button type="button" onclick="window.history.back()" class="genericBack">Back</button>
                <button id="btnSignUp" onclick="" class="genericForward">Sign up</button>
    </center>    
    
    <script type="text/javascript">
       
    </script>
        </form>
    </body>
</html>
