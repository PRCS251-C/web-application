<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Pizzariño - Simply perfect</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
input {

    font-size: 1.5vw; 
    font-family: 'Muli', sans-serif;
}
.goButton{
    background-color: rgba(50, 205, 50, 1);
    font-family: 'Muli', sans-serif;
    text-align: center;
    color: black;
    width: 150px;
    height: 55px;
    vertical-align: central;
    font-size: 26px;
    cursor: pointer;
    border-left-width: 0px;
    border-right-width: 0px;
    transition: 0.3s;
}
.goButton:Hover{
    background-color: rgba(100, 215, 255, 1);
}

.left{
    float: left;
    margin-left: 400px;
}
.right{
    float: right;
    margin-right: 400px;
}

.logostyle{
    max-width: 40%;
    max-height: 40%;
    height: auto;
    width: auto;
}

.imgInTable{
    vertical-align: middle;
    text-align: center;
    border-width: 5px;
    border-style: solid;
    border-color: lightgrey;
    border-radius: 10px;
    height: auto;
    width: auto;
    max-height: 100%;
    max-width: 100%;
    
}


        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body onload="rotate()">
        <form name="homePageForm" method="post" action="HomePageServlet" onkeypress="return event.keyCode != 13;">
            <center>
                <p><img class ="logostyle" id="homePageLogo" src="resources/images/whitelogo1.png"></p>
                <p class = "subtitle" style="color: white">Here at Pizzariño, we don't just create pizzas. We create perfection.</p>
                <p class = "title" style="color: white">Create your Pizzariño experience today</p>
                <table style="width:50%; height: 50px">
                    <tr>
                        <td style="width:100%;height:50px"><input required name="postcodeInput" type="text" placeholder="Please enter your postcode to begin" style="width:100%;height:50px"></td>
                        <td style="width:100%;height:49px"><button type="input" class="goButton" id="homePageBtnGo">GO</button</td>
                    </tr>
                </table>
                <img class="imgInTable" id="imgBanner" src="resources/images/pizzabanner1.png" alt=""/>
            </center>
        </form>        
        <script type="text/Javascript">
            bannersArray = new Array("resources/images/pizzabanner1.png", "resources/images/pizzabanner2.png", "resources/images/pizzabanner3.png");
            currentBanner = 0;
            
            function rotate() {
                currentBanner++;
                if (currentBanner == bannersArray.length){
                    currentBanner = 0;
                }
                document.getElementById("imgBanner").src = bannersArray[currentBanner];
                setTimeout("rotate()", 5000);
            }
        </script>
    </body>
</html>
