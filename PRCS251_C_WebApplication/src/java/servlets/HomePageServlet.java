/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;
 
import datamodel.ModelController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
 
 
/**
 *
 * @author Alex
 */
@WebServlet(name = "HomePageServlet", urlPatterns = {"/HomePageServlet"})
public class HomePageServlet extends HttpServlet {
 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
 
       
       
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        response.setContentType("text/html;charset=UTF-8");
        String postcode = request.getParameter("postcodeInput");
        String loggedIn = "0";
        postcode = postcode.toUpperCase();
        session.setAttribute("Postcode", postcode);
        session.setAttribute("loggedIn", loggedIn);
        Cookie postcodeCookie = new Cookie("Postcode", postcode);
        postcodeCookie.setMaxAge(-1);
        response.addCookie(postcodeCookie);  
       
        //delivery cookies
        Cookie FirstNameCookie = new Cookie("FirstName", "");
        Cookie LastNameCookie = new Cookie("LastName", "");
        Cookie EmailCookie = new Cookie("Email", "");
        Cookie PhoneNoCookie = new Cookie("PhoneNumber", "");
        Cookie Address1Cookie = new Cookie("AddressLine1", "");
        Cookie Address2Cookie = new Cookie("AddressLine2", "");
        Cookie CityCookie = new Cookie("City", "");
        //setting the life of the cookies
        FirstNameCookie.setMaxAge(-1);
        LastNameCookie.setMaxAge(-1);
        EmailCookie.setMaxAge(-1);
        PhoneNoCookie.setMaxAge(-1);
        Address1Cookie.setMaxAge(-1);
        Address2Cookie.setMaxAge(-1);
        CityCookie.setMaxAge(-1);
        //adding the cookies
        response.addCookie(FirstNameCookie);
        response.addCookie(LastNameCookie);
        response.addCookie(EmailCookie);
        response.addCookie(PhoneNoCookie);
        response.addCookie(Address1Cookie);
        response.addCookie(Address2Cookie);
        response.addCookie(CityCookie);
       
        try (PrintWriter out = response.getWriter()) {
            response.sendRedirect("Menu.jsp");        
            }
        }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
 
}
