<%@page import="api.APIConnection"%>
<%@page import="datamodel.*"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Confirm - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
html {
    overflow-y: scroll;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body onload="paymentTypeShow()">
        <form name="ConfirmOrderForm" method="post" action="ConfirmOrderServlet" onkeypress="return event.keyCode != 13;">
            <%
                                ArrayList<Product> productList = ModelController.getInstance().getProductList();
                Object[] productArray = productList.toArray();
                String productJson = APIConnection.mapper.writeValueAsString(productArray);
                String postcode = "";
                String firstName = "";
                String lastName = "";
                String email = "";
                String phoneNo= "";
                String address1 = "";
                String address2 = "";
                String city = "";
                String payPostcode = "";
                String payName = "";
                String payNum = "";
                String payCardType = "";
                String payCVC= "";
                String payIssue = "";
                String payExp = "";
                String payAddress = "";
                String payCity = "";
                String paymentType = "";
               Cookie cookie = null;
               Cookie[] cookies = null;
               cookies = request.getCookies();
               if( cookies != null ){
                  for (int i = 0; i < cookies.length; i++){
                     cookie = cookies[i];
                     if(cookie.getName().equals("Postcode")){
                         postcode = cookie.getValue();
                        }else if(cookie.getName().equals("FirstName")){
                            firstName = cookie.getValue();
                        }else if(cookie.getName().equals("LastName")){
                            lastName = cookie.getValue();
                        }else if(cookie.getName().equals("Email")){
                            email = cookie.getValue();
                        }else if(cookie.getName().equals("PhoneNumber")){
                            phoneNo = cookie.getValue();                
                        }else if(cookie.getName().equals("AddressLine1")){
                            address1 = cookie.getValue();
                        }else if(cookie.getName().equals("AddressLine2")){
                            address2 = cookie.getValue();
                        }else if(cookie.getName().equals("City")){ 
                            city = cookie.getValue();
                        }else if(cookie.getName().equals("CardPostcode")){
                            payPostcode = cookie.getValue();
                        }else if(cookie.getName().equals("CardName")){
                            payName = cookie.getValue();
                        }else if(cookie.getName().equals("CardNum")){
                            payNum = cookie.getValue();
                        }else if(cookie.getName().equals("CardType")){
                            payCardType = cookie.getValue();
                        }else if(cookie.getName().equals("CardCVC")){
                            payCVC = cookie.getValue();                
                        }else if(cookie.getName().equals("CardIssue")){
                            payIssue = cookie.getValue();
                        }else if(cookie.getName().equals("CardExp")){
                            payExp = cookie.getValue();
                        }else if(cookie.getName().equals("CardAddress")){ 
                            payAddress = cookie.getValue();
                        }else if(cookie.getName().equals("CardCity")){ 
                            payCity = cookie.getValue();
                        }else if(cookie.getName().equals("PaymentType")){ 
                            paymentType = cookie.getValue();
                        }
                    }
                }
               
   %>
            <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                        </th>
                    </tr>
               </table>                        
                
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';">Basket</td>            
            </table>
    <center>
        <table style="width:75%;height:50px">
            <tr>
                <th class="orderProgress done" onclick="location.href='Basket.jsp';">BASKET</th>
                <th class="orderProgress done" onclick="location.href='Delivery.jsp';">DELIVERY DETAILS</th>
                <th class="orderProgress done" onclick="location.href='Payment.jsp';">PAYMENT DETAILS</th>
                <th class="orderProgress inProg">ORDER REVIEW</th>
            </tr>
        </table>
        <p class="title" style="color: white">Order Review</p>
        <table class="checkoutTable">
        <tr>
          <td class="confirmTableContent" rowspan="2" style="width: 50%">
           <p class="title" style="color: white" id="subtotal">Subtotal: £££</p>
                    <p class="subtitle" id="orderItems">
                        Pepperoni Pizza - Large</br>
                        Cheesy Garlic Bread x1</br>
                        Diet Pepsi 2L x1
                    </p>
          </td>
          <td class="confirmTableContent">
          <p class="title" style="color: white">Customer Information</p>
        <span class="subtitle" style="float: left">First Name: </span>
            <span class="subtitle" style="float: right"><%=firstName%></span></br>
            
        <span class="subtitle" style="float: left">Last Name: </span>
            <span class="subtitle" style="float: right"><%=lastName%></span></br>
            
        <span class="subtitle" style="float: left">Email Address: </span>
            <span class="subtitle" style="float: right"><%=email%></span></br>
            
        <span class="subtitle" style="float: left">Phone Number: </span>
            <span class="subtitle" style="float: right"><%=phoneNo%></span></br>
            
        <span class="subtitle" style="float: left">Address Line 1: </span>
            <span class="subtitle" style="float: right"><%=address1%></span></br>
            
        <span class="subtitle" style="float: left">Address Line 2: </span>
            <span class="subtitle" style="float: right"><%=address2%></span></br>
            
        <span class="subtitle" style="float: left">City: </span>
            <span class="subtitle" style="float: right"><%=city%></span></br>
        
        <span class="subtitle" style="float: left">Postcode: </span>
            <span class="subtitle" style="float: right"><%=postcode%></span></br>
            </td>
        </tr>
        <tr>
          <td class="confirmTableContent">
              <p class="title" style="color: white">Payment Information</p>
        <span class="subtitle" style="float: left">Payment Type: </span>
            <span class="subtitle" id="paymentTypeField" style="float: right"><%=paymentType%></span></br>
        <div id="confirmCard" style="display: block">
        <span class="subtitle" style="float: left">Name on Card: </span>
            <span class="subtitle" style="float: right"><%=payName%></span></br>
        <span class="subtitle" style="float: left">Card Number: </span>
            <span class="subtitle" style="float: right" id="cardNumDisplay"></span></br> 
        <span class="subtitle" style="float: left">Card Type: </span>
            <span class="subtitle" style="float: right"><%=payCardType%></span></br>       
        <span class="subtitle" style="float: left">CVC: </span>
            <span class="subtitle" style="float: right"><%=payCVC%></span></br>
        <span class="subtitle" style="float: left">Issue Number: </span>
            <span class="subtitle" style="float: right"><%=payIssue%></span></br>
        <span class="subtitle" style="float: left">Card Expiry Date: </span>
            <span class="subtitle" style="float: right"><%=payExp%></span></br>
        <span class="subtitle" style="float: left">Billing Address Line 1: </span>
            <span class="subtitle" style="float: right"><%=payAddress%></span></br>
        <span class="subtitle" style="float: left">Billing Address City: </span>
            <span class="subtitle" style="float: right"><%=payCity%></span></br>
        <span class="subtitle" style="float: left">Billing Postcode: </span>
            <span class="subtitle" style="float: right"><%=payPostcode%></span></br>
        </div>
        <div id="confirmCash" style="display: none">
            <span class="subtitle">To pay:</span>   
            <p class="title"> [price]</p></br>
            <span class="subtitle">on delivery.</span> 
        </div>
          </td>
        </tr>
      </table>

        </br>
        </br>   
            
            
        </br>
            <button type="button" onclick="location.href='Payment.jsp';" class="genericBack">◄ Back to payment</button>
            <button onclick="location.href='OrderConfirmation.jsp';" class="genericForward">Place order ► </button>
    </center>
          <script type="text/javascript">
//            function last4(){
//                
//                var cardNum = <%=payNum%>;
//               // var cardNumArray = cardNum.split(/\d{4}(?=\d{4})/);
////                for (var i = 0; i < cardNumArray.length - 1; i++) {
////                    cardNum = cardNum + cardNumArray[i].replace(/\[0-9]/, "*") + " ";
////                }
////                cardNum = cardNum + cardNumArray[3];
////                alert(cardNum);
//                document.getElementById("cardNumDisplay").innerText=cardNum;
//            }

            var products = new Array();
            
            var orderItemsCookie = getCookie("orderItems");
                
            if (orderItemsCookie === "") {
                var orderItems = new Array();
            } else {
                var orderItems = JSON.parse(orderItemsCookie);
            }
                
                
            products = <%=productJson%>;
            populateBasket();
            
            function populateBasket() {
                html = "";

                var totalCost = 0.0;
                
                for (var i = 0; i < orderItems.length; i++) {
                    var orderId = orderItems[i].ORDER_ID;
                    var productId = 0;
                    var productType = "";
                    var productName = "";
                    var productPrice = "";
                    var productSize = "";
                    var productQuantity = "";
                    
                    for (var j = 0; j < products.length; j++) {
                        if (products[j].PRODUCT_ID.toString() === orderItems[i].PRODUCT_ID) {
                            productType = products[j].PRODUCT_TYPE;
                            productName = products[j].PRODUCT_NAME;
                            for (var k = 0; k < products[j].PRODUCT_SIZES.length; k++) {
                                if (products[j].PRODUCT_SIZES[k].SIZE_ID.toString() === orderItems[i].ORDER_ITEM_SIZE.toString()) {
                                    productPrice = products[j].PRODUCT_SIZES[k].PRICE;
                                    productSize = products[j].PRODUCT_SIZES[k].sizeName;
                                    productQuantity = orderItems[i].ORDER_ITEM_QUANTITY;
                                }
                            }
                        }
                    }
                    var price = (parseFloat(productPrice) * parseFloat(productQuantity));
                    totalCost = totalCost + price;
                    
                    if (productType === "Pizza" || productType === "Drink") {
                        html = html + "" + productName + " - " + productSize + " x" + productQuantity + "<br>";
                    } else {
                        html = html + "" + productName + " x" + productQuantity + "<br>";
                    }
                }
                
                html = html + "";
                
                document.getElementById("orderItems").innerHTML = html;
                
                document.getElementById("subtotal").innerHTML = "Subtotal: £" + totalCost.toFixed(2).toString();
                
            }
            
            function getCookie(cookieName) {
                var name = cookieName + "=";
                var cookieArray = document.cookie.split(";");

                for (var i = 0; i < cookieArray.length; i++) {
                    var cookie = cookieArray[i];

                    while (cookie.charAt(0) === ' ') {
                        cookie = cookie.substring(1);
                    }

                    if (cookie.indexOf(name) === 0) {
                        return decodeURIComponent(cookie.substring(name.length, cookie.length));
                    }
                }

                return "";
            }  
            
                function paymentTypeShow(){
                    var paymentTypeJS = paymentTypeField.innerText;
                    if (paymentTypeJS === "Card") {
                        confirmCard.style.display = 'block';
                        confirmCash.style.display = 'none';
                    } else if (paymentTypeJS === "Cash") {
                        confirmCard.style.display = 'none';
                        confirmCash.style.display = 'block';
                    }
                }

        </script>
        </form>
    </body>
</html>
