<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Payment - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
html {
    overflow-y: scroll;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css?id=1">
    </head>
    <body>
        <form name="OrderConfirmForm" method="post" action="OrderConfirmServlet" onkeypress="return event.keyCode != 13;">
            <%           
   Cookie cookie = null;
   Cookie[] cookies = null;
   String postcode = "no";
   cookies = request.getCookies();
   if( cookies != null ){
      for (int i = 0; i < cookies.length; i++){
         cookie = cookies[i];
         if(cookie.getName().equals("Postcode")){
             postcode = cookie.getValue();
         }
      }
  }
   %>
        <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                        </th>
                    </tr>
               </table>           
                
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';" id="baskettable">Basket</td>            
            </table>
    <center>
        <table style="width:75%;height:50px">
            <tr>
                <th class="orderProgress complete">BASKET</th>
                <th class="orderProgress complete">DELIVERY DETAILS</th>
                <th class="orderProgress complete">PAYMENT DETAILS</th>
                <th class="orderProgress complete">ORDER REVIEW</th>
            </tr>
        </table>
        
        </br>
        <p class="title" style="color: white">your order has been placed - order summary</p>
    </center>
        </form>
    </body>
</html>

