<%@page import="api.APIConnection"%>
<%@page import="datamodel.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datamodel.ModelController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Basket - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <style type = "text/css">
html {
    overflow-y: scroll;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body>
        <form name="basketForm" method="post" action="BasketServlet" onkeypress="return event.keyCode != 13;">
            <% 
                
                ArrayList<Product> productList = ModelController.getInstance().getProductList();
                Object[] productArray = productList.toArray();
                String productJson = APIConnection.mapper.writeValueAsString(productArray);
   Cookie cookie = null;
   Cookie[] cookies = null;
   String postcode = "no";
   cookies = request.getCookies();
   if( cookies != null ){
      for (int i = 0; i < cookies.length; i++){
         cookie = cookies[i];
         if(cookie.getName().equals("Postcode")){
             postcode = cookie.getValue();
         }
      }
  }
   %>
        <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                                <span>Delivering to: </br>
                                <%=postcode%></span></br>                   
                            <button type="button" class="genericButton1" align="right" id="postcodeChangeBtn" style="height: 30px; width: 250px; font-size: 20px;" onclick="location.href='ChangePostcode.jsp';">Change?</button>
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                        </th>
                    </tr>
               </table>
                
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar selected" onclick="location.href='Basket.jsp';">Basket</td>            
            </table>
    <center>
        <table style="width:75%;height:50px">
            <tr>
                <th class="orderProgress inProg">BASKET</th>
                <th class="orderProgress">DELIVERY DETAILS</th>
                <th class="orderProgress">PAYMENT DETAILS</th>
                <th class="orderProgress">ORDER REVIEW</th>
            </tr>
        </table>
            <p class="title" style="color: white">Basket</p>
            <p class="biggerTitle" style="color: white">Here's a list of everything you've ordered:</p></br></br>
            <div id="basketContainer" class ="basketText">             
                <table class="basketContainer">
              <tr>
                <th width="50%"><div class="basketTextInner">Pizza Name</div></th>
                <th rowspan="3"><div class="basketTextInner">Price</div></th>
                <th rowspan="3"><div class="basketTextInner smallRight"><span class="deleteBtn">X</span></div></th>
              </tr>
              <tr>
                <td><div class="basketTextInner small">Size</div></td>
              </tr>
              <tr>
                <td><div class="basketTextInner small">Quantity</div></td>
              </tr>
            </table>
            
            
            <table class="basketContainer">
                <tr>
                  <th width="50%"><div class="basketTextInner">Side Name</div></th>
                  <th rowspan="2"><div class="basketTextInner">Price</div></th>
                  <th rowspan="2"><div class="basketTextInner smallRight"><span class="deleteBtn">X</span></th>
                </tr>
                <tr>
                  <td><div class="basketTextInner small">Quantity</div></td>
                </tr>
            </table>
                </div>
            <p class="title" id="subtotal" style="color: white;">SUBTOTAL: £££</p>
    </b>
    </b>
    </b>
        <button type="button" onclick="location.href='Menu.jsp';" class="genericBack">◄ Back to menu</button>
        <button onclick="location.href='Delivery.jsp';" class="genericForward">Checkout ►</button>
        <span hidden id="hiddenSpan"><%=productJson%></span>
        <br><br>
    </center>
        <script type="text/javascript">

            var products = new Array();
            
            orderItemsCookie = getCookie("orderItems");
                
            if (orderItemsCookie === "") {
                var orderItems = new Array();
            } else {
                var orderItems = JSON.parse(orderItemsCookie);
            }
                
                
            products = <%=productJson%>;
            populateBasket();
            
            function populateBasket() {
                html = "";

                var totalCost = 0.0;
                
                for (var i = 0; i < orderItems.length; i++) {
                    var orderId = orderItems[i].ORDER_ID;
                    var productId = 0;
                    var productType = "";
                    var productName = "";
                    var productPrice = "";
                    var productSize = "";
                    var productQuantity = "";
                    
                    for (var j = 0; j < products.length; j++) {
                        if (products[j].PRODUCT_ID.toString() === orderItems[i].PRODUCT_ID) {
                            productType = products[j].PRODUCT_TYPE;
                            productName = products[j].PRODUCT_NAME;
                            for (var k = 0; k < products[j].PRODUCT_SIZES.length; k++) {
                                if (products[j].PRODUCT_SIZES[k].SIZE_ID.toString() === orderItems[i].ORDER_ITEM_SIZE.toString()) {
                                    productPrice = products[j].PRODUCT_SIZES[k].PRICE;
                                    productSize = products[j].PRODUCT_SIZES[k].sizeName;
                                    productQuantity = orderItems[i].ORDER_ITEM_QUANTITY;
                                }
                            }
                        }
                    }
                    var price = (parseFloat(productPrice) * parseFloat(productQuantity));
                    totalCost = totalCost + price;
                    
                    if (productType === "Pizza" || productType === "Drink") {
                        html = html + '<table class="basketContainer" id="' + orderItems[i].PRODUCT_ID + '">';
                        html = html + '<tr>';
                        html = html + '<th width="50%"><div class="basketTextInner">' + productName + '</div></th>';
                        html = html + '<th rowspan="3"><div class="basketTextInner">£' + price.toFixed(2).toString() + '</div></th>';
                        html = html + '<th rowspan="3"><div class="basketTextInner smallRight"><span class="deleteBtn">X</span></div></th>';
                        html = html + '</tr>';
                        html = html + '<tr>';
                        html = html + '<td><div class="basketTextInner small">' + productSize + '</div></td>';
                        html = html + '</tr>';
                        html = html + '<tr>';
                        html = html + '<td><div class="basketTextInner small">Quantity: ' + productQuantity + '</div></td>';
                        html = html + '</tr>';
                        html = html + '</table>';
                    } else {
                        html = html + '<table class="basketContainer" id="' + orderItems[i].PRODUCT_ID + '">';
                        html = html + '<tr>';
                        html = html + '<th width="50%"><div class="basketTextInner">' + productName + '</div></th>';
                        html = html + '<th rowspan="2"><div class="basketTextInner">£' + price.toFixed(2).toString() + '</div></th>';
                        html = html + '<th rowspan="2"><div class="basketTextInner smallRight"><span class="deleteBtn">X</span></th>';
                        html = html + '</tr>';
                        html = html + '<tr>';
                        html = html + '<td><div class="basketTextInner small">Quantity: ' + productQuantity + '</div></td>';
                        html = html + '</tr>';
                        html = html + '</table>';
                    }
                }
                
                document.getElementById("basketContainer").innerHTML = html;
                
                document.getElementById("subtotal").innerHTML = "SUBTOTAL: £" + totalCost.toFixed(2).toString();
                
            }
            
            function getCookie(cookieName) {
                var name = cookieName + "=";
                var cookieArray = document.cookie.split(";");

                for (var i = 0; i < cookieArray.length; i++) {
                    var cookie = cookieArray[i];

                    while (cookie.charAt(0) === ' ') {
                        cookie = cookie.substring(1);
                    }

                    if (cookie.indexOf(name) === 0) {
                        return decodeURIComponent(cookie.substring(name.length, cookie.length));
                    }
                }

                return "";
            }
            
            function setCookie(cookieName, jsonObject) {
                document.cookie = cookieName + "=" + encodeURIComponent(jsonObject) + ";" + "expires=-1;path=/";
            }
            
            $(".deleteBtn").on("click", function(){
                var confirmDel = confirm("Are you sure you want to remove this item from your basket?");
                if (confirmDel === true){
                    $(this).closest("table").remove();
                    var id = $(this).closest("table").attr("id");
                    var index = 0;
                    
                    for (var i = 0; i < orderItems.length; i++) {
                        if (orderItems[i].PRODUCT_ID.toString() === id.toString()) {
                            index = i;
                            break;
                        }
                    }
                    
                    orderItems.splice(index, 1);
                    
                    setCookie("orderItems", JSON.stringify(orderItems));
                }
            }); 
            
        </script>
        </form>
    </body>
    
</html>

