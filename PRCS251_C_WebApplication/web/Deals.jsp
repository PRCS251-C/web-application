<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Deals - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <style type = "text/css">
html {
    overflow-y: auto;
}

        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body>
        <form name="DealsForm" method="post" action="DealsServlet" onkeypress="return event.keyCode != 13;">
            <%           
   Cookie cookie = null;
   Cookie[] cookies = null;
   String postcode = "";
   cookies = request.getCookies();
   if( cookies != null ){
      for (int i = 0; i < cookies.length; i++){
         cookie = cookies[i];
         if(cookie.getName().equals("Postcode")){
             postcode = cookie.getValue();
         }
      }
  }
   %>
            <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                                <span>Delivering to: </br>
                                <%=postcode%></span></br>                   
                            <button type="button" class="genericButton1" align="right" id="postcodeChangeBtn" style="height: 30px; width: 250px; font-size: 20px;" onclick="location.href='ChangePostcode.jsp';">Change?</button>
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                        </th>
                    </tr>
               </table>             
                
        <div class="fixToTop">       
        <table style="width: inherit" id="navBarTable">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar selected" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';" id="baskettable">Basket: 0</td>            
            </table>     
        </div>
            <div class="stopJumpDiv"></div>
    <center>
        <div style="width: 70%">
        <p class="biggerTitle fancy" style="color: white">Deals</p>

        <div class="dealsTable">        
                <div class="menuName deals">Test Deal 2</div>
                <div class="dealsText"><div class="subtitle margin">Buy one get one free on any large pizza</div></div>
                <div class="menuAdd deals">Create your deal</div>
        </div>
        <div class="dealsTable">        
            <div class="menuName deals">Test Deal 1</div>
            <div class="dealsText"><div class="subtitle margin">I am screaming. I am screaming and I cannot screaming. Someone help me, it hurts too much to bare.</div></div>
            <div class="menuAdd deals">Create your deal</div>
        </div>
        <div class="dealsTable">        
            <div class="menuName deals">Test Deal 3</div>
            <div class="dealsText"><div class="subtitle margin">This is a deal to test the deals. This is a large pizza, a side and a drink for £14.99</div></div>
            <div class="menuAdd deals">Create your deal</div>
        </div>
        <div class="dealsTable">        
            <div class="menuName deals">Test Deal 4</div>
            <div class="dealsText"><div class="subtitle margin">This is a deal to test the deals. Small pizza for £5</div></div>
            <div class="menuAdd deals">Create your deal</div>
        </div>

        </div>
    </center>
    <script src="resources/basket.js" type="text/javascript"></script>
<script>
var fixmeTop = $('.fixToTop').offset().top;
var itemsInBasket = 0;
$(window).scroll(function() {
    var currentScroll = $(window).scrollTop();
    if (currentScroll >= fixmeTop) {
        $('.fixToTop').css({
            position: 'fixed',
            top: '0',
        });
        $('.stopJumpDiv').css({
            height: '96px'
        });
    } else {
        $('.fixToTop').css({
            position: 'static' 
        });
        $('.stopJumpDiv').css({
            height: '0px'
        });
    }
});
</script>
        </form>
    </body>
</html>


