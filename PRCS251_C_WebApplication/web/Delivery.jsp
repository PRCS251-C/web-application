<%@page import="api.APIConnection"%>
<%@page import="datamodel.*"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Checkout - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
html {
    overflow-y: scroll;
}
input{
    width: 60%;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css?id=1">
    </head>
    <body>
        <form name="checkoutForm" method="post" action="CheckoutServlet" onkeypress="return event.keyCode != 13;">
<%           
    
                ArrayList<Product> productList = ModelController.getInstance().getProductList();
                Object[] productArray = productList.toArray();
                String productJson = APIConnection.mapper.writeValueAsString(productArray);
    Cookie cookie = null;
    Cookie[] cookies = null;
    String postcode = "";
    String firstName = "";
    String lastName = "";
    String email = "";
    String phoneNo= "";
    String address1 = "";
    String address2 = "";
    String city = "";
    cookies = request.getCookies();
    if( cookies != null ){
        for (int i = 0; i < cookies.length; i++){
            cookie = cookies[i];
            if(cookie.getName().equals("Postcode")){
                postcode = cookie.getValue();
            }else if(cookie.getName().equals("FirstName")){
                firstName = cookie.getValue();
            }else if(cookie.getName().equals("LastName")){
                lastName = cookie.getValue();
            }else if(cookie.getName().equals("Email")){
                email = cookie.getValue();
            }else if(cookie.getName().equals("PhoneNumber")){
                phoneNo = cookie.getValue();                
            }else if(cookie.getName().equals("AddressLine1")){
                address1 = cookie.getValue();
            }else if(cookie.getName().equals("AddressLine2")){
                address2 = cookie.getValue();
            }else if(cookie.getName().equals("City")){ 
                city = cookie.getValue();
            }
        }
   }
%>
        <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                                <div class="title" style="color: white; vertical-align: top">
                                <span>Delivering to: </br>
                                <%=postcode%></span></br>                   
                            <button type="button" class="genericButton1" align="right" id="postcodeChangeBtn" style="height: 30px; width: 250px; font-size: 20px;" onclick="location.href='ChangePostcode.jsp';">Change?</button>
                           </div>
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                        </th>
                    </tr>
               </table>               
                
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';">Basket</td>            
            </table>
    <center>
        <table style="width:75%;height:50px">
            <tr>
                <th class="orderProgress done" onclick="location.href='Basket.jsp';">BASKET</th>
                <th class="orderProgress inProg">DELIVERY DETAILS</th>
                <th class="orderProgress">PAYMENT DETAILS</th>
                <th class="orderProgress">ORDER REVIEW</th>
            </tr>
        </table>
        <p class="title" style="color: white">Delivery Details</p>

        <table class="checkoutTable" style="text-align: center">
            <tr>
                <td class="checkoutTableContent">
                    <p class="title" id="subtotal">Subtotal: £££</p>
                    <p class="subtitle" id="orderItems">
                        Pepperoni Pizza - Large</br>
                        Cheesy Garlic Bread x1</br>
                        Diet Pepsi 2L x1
                    </p>
                </td>
                <td class="checkoutTableContent">
                    <div id="checkoutInfo">
                        </br>
                        </br>
                        <span class="subtitle" style="float: left">First Name: </span>
                            <input required type="text" name="checkoutFirstName" value="<%=firstName%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Last Name: </span>
                            <input required type="text" name="checkoutLastName" value="<%=lastName%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Email Address: </span>
                            <input required type="text" name="checkoutEmail" value="<%=email%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Phone Number: </span>
                            <input required type="text" name="checkoutPhNo" value="<%=phoneNo%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Address Line 1: </span>
                            <input required type="text" name="checkoutAddress1" value="<%=address1%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Address Line 2: </span>
                            <input required type="text" name="checkoutAddress2" value="<%=address2%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">City: </span>
                            <input required type="text" name="checkoutCity" value="<%=city%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Postcode: </span>
                        <span  style="width:60%; float: right"><%=postcode%></span><br><br>
                        <span class="subtitle" style="float: left">Use delivery address as payment address?</span>
                            <input required type="checkbox" name="useDeliveryAddress" value="True" class="checkboxStyle">
                    </div>                      
                               
            </tr>
            </table>  
                <button type="button" onclick="location.href='Basket.jsp';" class="genericBack"> ◄ Back to basket</button></th>
                <button id="btnChangeDiv" onclick="location.href='Payment.jsp';" class="genericForward">Payment Details ►</button></th>
            
          
            
    </center>    
    
    <script type="text/javascript">
            var products = new Array();
            
            var orderItemsCookie = getCookie("orderItems");
                
            if (orderItemsCookie === "") {
                var orderItems = new Array();
            } else {
                var orderItems = JSON.parse(orderItemsCookie);
            }
                
                
            products = <%=productJson%>;
            populateBasket();
            
            function populateBasket() {
                html = "";

                var totalCost = 0.0;
                
                for (var i = 0; i < orderItems.length; i++) {
                    var orderId = orderItems[i].ORDER_ID;
                    var productId = 0;
                    var productType = "";
                    var productName = "";
                    var productPrice = "";
                    var productSize = "";
                    var productQuantity = "";
                    
                    for (var j = 0; j < products.length; j++) {
                        if (products[j].PRODUCT_ID.toString() === orderItems[i].PRODUCT_ID) {
                            productType = products[j].PRODUCT_TYPE;
                            productName = products[j].PRODUCT_NAME;
                            for (var k = 0; k < products[j].PRODUCT_SIZES.length; k++) {
                                if (products[j].PRODUCT_SIZES[k].SIZE_ID.toString() === orderItems[i].ORDER_ITEM_SIZE.toString()) {
                                    productPrice = products[j].PRODUCT_SIZES[k].PRICE;
                                    productSize = products[j].PRODUCT_SIZES[k].sizeName;
                                    productQuantity = orderItems[i].ORDER_ITEM_QUANTITY;
                                }
                            }
                        }
                    }
                    var price = (parseFloat(productPrice) * parseFloat(productQuantity));
                    totalCost = totalCost + price;
                    
                    if (productType === "Pizza" || productType === "Drink") {
                        html = html + "" + productName + " - " + productSize + " x" + productQuantity + "<br>";
                    } else {
                        html = html + "" + productName + " x" + productQuantity + "<br>";
                    }
                }
                
                html = html + "";
                
                document.getElementById("orderItems").innerHTML = html;
                
                document.getElementById("subtotal").innerHTML = "Subtotal: £" + totalCost.toFixed(2).toString();
                
            }
            
            function getCookie(cookieName) {
                var name = cookieName + "=";
                var cookieArray = document.cookie.split(";");

                for (var i = 0; i < cookieArray.length; i++) {
                    var cookie = cookieArray[i];

                    while (cookie.charAt(0) === ' ') {
                        cookie = cookie.substring(1);
                    }

                    if (cookie.indexOf(name) === 0) {
                        return decodeURIComponent(cookie.substring(name.length, cookie.length));
                    }
                }

                return "";
            }  
    </script>
        </form>
    </body>
</html>
