<%@page import="api.APIConnection"%>
<%@page import="datamodel.*"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Payment - Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
html {
    overflow-y: scroll;
}
input{
    width: 60%;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body onload="document.getElementById('payCard').click()">
         <form name="PaymentForm" method="post" action="PaymentServlet" onkeypress="return event.keyCode != 13;">
             <%       
                ArrayList<Product> productList = ModelController.getInstance().getProductList();
                Object[] productArray = productList.toArray();
                String productJson = APIConnection.mapper.writeValueAsString(productArray);
 Cookie cookie = null;
    Cookie[] cookies = null;
    String payPostcode = "";
    String payName = "";
    String payNum = "";
    String payCardType = "";
    String payCVC= "";
    String payIssue = "";
    String payExp = "";
    String payAddress = "";
    String payAddress1 = "";
    String payAddress2 = "";
    String payCity = "";
    String checked = (String)session.getAttribute("CheckedBox");
    String postcode = "";
    String phoneNo= "";
    String address1 = "";
    String address2 = "";
    String city = "";
    cookies = request.getCookies();
   
   
    if( cookies != null ){
        for (int i = 0; i < cookies.length; i++){
            cookie = cookies[i];
            if(cookie.getName().equals("CardPostcode")){
                payPostcode = cookie.getValue();
            }else if(cookie.getName().equals("CardName")){
                payName = cookie.getValue();
            }else if(cookie.getName().equals("CardNum")){
                payNum = cookie.getValue();
            }else if(cookie.getName().equals("CardType")){
                payCardType = cookie.getValue();
            }else if(cookie.getName().equals("CardCVC")){
                payCVC = cookie.getValue();                
            }else if(cookie.getName().equals("CardIssue")){
                payIssue = cookie.getValue();
            }else if(cookie.getName().equals("CardExp")){
                payExp = cookie.getValue();
            }else if(cookie.getName().equals("CardAddress")){
                payAddress = cookie.getValue();
            }else if(cookie.getName().equals("CardCity")){
                payCity = cookie.getValue();
            }else if(cookie.getName().equals("Postcode")){
                postcode = cookie.getValue();
            }else if(cookie.getName().equals("AddressLine1")){
                address1 = cookie.getValue();
            }else if(cookie.getName().equals("AddressLine2")){
                address2 = cookie.getValue();
            }else if(cookie.getName().equals("City")){
                city = cookie.getValue();
            }
        }
               
       
        }
    if (checked.equals("true")){
            payAddress1 = address1;
            payAddress2 = address2;
            payCity = city;
            payPostcode = postcode;
            payAddress = payAddress1 + ", " + payAddress2;            
        }
   %>
        <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                        </th>
                    </tr>
               </table>                          
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';">Basket</td>            
            </table>
        <center>
            <table style="width:75%;height:50px">
            <tr>
                <th class="orderProgress done" onclick="location.href='Basket.jsp';">BASKET</th>
                <th class="orderProgress done" onclick="location.href='Delivery.jsp';">DELIVERY DETAILS</th>
                <th class="orderProgress inProg">PAYMENT DETAILS</th>
                <th class="orderProgress">ORDER REVIEW</th>
            </tr>
        </table>
            <p class="title" style="color: white">Payment Details</p>
            <table class="checkoutTable" style="text-align: center">
            <tr>
                <td class="checkoutTableContent">
                    <p class="title" id="subtotal">Subtotal: £££</p>
                    <p class="subtitle" id="orderItems">
                        Pepperoni Pizza - Large</br>
                        Cheesy Garlic Bread x1</br>
                        Diet Pepsi 2L x1
                    </p>
                </td>
                <td class="checkoutTableContent">                          
                        <div class="tab">
                            <button id="payCard" type="button" class="tablinks" onclick="paymentButtonCard(event, 'divCard');">Pay with Card</button>
                            <button id="payCash" type="button" class="tablinks" onclick="paymentButtonCash(event, 'divCash');">Pay with Cash</button> 
                        </div>
                    <div id="divCard" class="tabcontent">
                        </br>
                        </br>
                        <span class="subtitle" style="float: left">Name on Card: </span>
                        <input required type="text" id="paymentName" name="paymentName" value="<%=payName%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Card Number: </span>
                        <input required type="number" id="paymentCardNo" name="paymentCardNo" value="<%=payNum%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Card Type: </span>
                        <select id="paymentCardType" name="paymentCardType" style="width:60%; float: right">
                            <option value="VISA Debit">VISA Debit</option>
                            <option value="VISA">VISA</option>
                            <option value="MasterCard">MasterCard</option>
                            <option value="American Express">American Express</option>
                        </select></br></br>
                        <span class="subtitle" style="float: left">CVC: </span>
                        <input required type="number" id="paymentCVC" name="paymentCVC" value="<%=payCVC%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Issue Number: </span>
                        <input required type="number" id="paymentIssue" name="paymentIssue" value="<%=payIssue%>" style="width:60%; float: right"></br></br> 
                        <span class="subtitle" style="float: left">Card Expiry Date: </span>
                        <input required type="text" id="paymentExp" name="paymentExp" value="<%=payExp%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Billing Address Line 1: </span>
                        <input required type="text" id="paymentAddr1" name="paymentAddress" value="<%=payAddress%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Billing Address City: </span>
                        <input required type="text" id="paymentCity" name="paymentCity" value="<%=payCity%>" style="width:60%; float: right"></br></br>
                        <span class="subtitle" style="float: left">Billing Postcode: </span>
                        <input required type="text" id="paymentPostcode" name="paymentPostcode" value="<%=payPostcode%>" style="width:60%; float: right"></br></br>                   
                    </div>
                    <div id="divCash" class="tabcontent">
                        <span class="title">Please pay your courier</span>   
                        <p class="biggerTitle"> [price]</p></br>
                        <span class="title">on delivery.</span> 
                    </div>                        
                    
                </td>                
            </tr>
            </table>  
               <button type="button" onclick="location.href='Delivery.jsp';" class="genericBack"> ◄ Back to delivery</button>
               <button id="btnPayment" onclick="location.href='ConfirmOrder.jsp';" class="genericForward">Order Review ►</button>
            
                  
        </center>
<input type="text" name="paymentTypeP" id="paymentTypeP" hidden>
        
    
      
    <script type="text/javascript">
            var products = new Array();
            
            var orderItemsCookie = getCookie("orderItems");
                
            if (orderItemsCookie === "") {
                var orderItems = new Array();
            } else {
                var orderItems = JSON.parse(orderItemsCookie);
            }
                
                
            products = <%=productJson%>;
            populateBasket();
            
            function populateBasket() {
                html = "";

                var totalCost = 0.0;
                
                for (var i = 0; i < orderItems.length; i++) {
                    var orderId = orderItems[i].ORDER_ID;
                    var productId = 0;
                    var productType = "";
                    var productName = "";
                    var productPrice = "";
                    var productSize = "";
                    var productQuantity = "";
                    
                    for (var j = 0; j < products.length; j++) {
                        if (products[j].PRODUCT_ID.toString() === orderItems[i].PRODUCT_ID) {
                            productType = products[j].PRODUCT_TYPE;
                            productName = products[j].PRODUCT_NAME;
                            for (var k = 0; k < products[j].PRODUCT_SIZES.length; k++) {
                                if (products[j].PRODUCT_SIZES[k].SIZE_ID.toString() === orderItems[i].ORDER_ITEM_SIZE.toString()) {
                                    productPrice = products[j].PRODUCT_SIZES[k].PRICE;
                                    productSize = products[j].PRODUCT_SIZES[k].sizeName;
                                    productQuantity = orderItems[i].ORDER_ITEM_QUANTITY;
                                }
                            }
                        }
                    }
                    var price = (parseFloat(productPrice) * parseFloat(productQuantity));
                    totalCost = totalCost + price;
                    
                    if (productType === "Pizza" || productType === "Drink") {
                        html = html + "" + productName + " - " + productSize + " x" + productQuantity + "<br>";
                    } else {
                        html = html + "" + productName + " x" + productQuantity + "<br>";
                    }
                }
                
                html = html + "";
                
                document.getElementById("orderItems").innerHTML = html;
                
                document.getElementById("subtotal").innerHTML = "Subtotal: £" + totalCost.toFixed(2).toString();
                
            }
            
            function getCookie(cookieName) {
                var name = cookieName + "=";
                var cookieArray = document.cookie.split(";");

                for (var i = 0; i < cookieArray.length; i++) {
                    var cookie = cookieArray[i];

                    while (cookie.charAt(0) === ' ') {
                        cookie = cookie.substring(1);
                    }

                    if (cookie.indexOf(name) === 0) {
                        return decodeURIComponent(cookie.substring(name.length, cookie.length));
                    }
                }

                return "";
            }    
    
        var paymentTypeVar;
        function paymentButtonCard(evt, divName){
            paymentTypeVar = "Card";
            document.getElementById("paymentTypeP").value = paymentTypeVar;
            changePayment(evt, divName);
        };
        
        function paymentButtonCash(evt, divName){
            paymentTypeVar = "Cash";
            document.getElementById("paymentTypeP").value = paymentTypeVar;
            changePayment(evt, divName);
        };
        
        function changePayment(evt, paymentType) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(paymentType).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
         </form>
    </body>
</html>