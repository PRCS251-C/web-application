/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import api.APIConnection;
import datamodel.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;

/**
 *
 * @author ndrichardson1
 */
@WebServlet(name = "ConfirmOrderServlet", urlPatterns = {"/ConfirmOrderServlet"})
public class ConfirmOrderServlet extends HttpServlet {

    //Order info
    String addressLine1 = "";
    String addressLine2 = "";
    String city = "";
    String address = "";
    String postcode = "";
    Integer orderId = 0;
    Double price = 0.0;
    LocalDateTime time = LocalDateTime.now();
    String status = "Order Placed";
    ArrayList<OrderItem> orderItems = new ArrayList<>();
    
    //Customer info
    Integer customerId = 0;
    String firstName = "";
    String lastName = "";
    String email = "";
    String phoneNumber = "";
    String customerAddressLine1 = "";
    String customerAddressLine2 = "";
    String customerCity = "";
    String customerAddress = "";
    String customerPostcode = "";
    Integer loggedIn = 0;
    String username = "";
    String password = "";
    
    //Payment info
    Integer paymentId = 0;
    String paymentMethod = "";
    String cardNumber = "";
    Integer cardCvc = 0;
    String cardType = "";
    String cardName = "";
    Integer cardIssueNumb = 0;
    String cardExpiryDate = "";
    String cardAddress = "";
    String cardPostcode = "";
    String cardCity = "";
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals("AddressLine1")) {
                        addressLine1 = cookie.getValue();
                    } else if (cookie.getName().equals("AddressLine2")) {
                        addressLine2 =  cookie.getValue();
                    } else if (cookie.getName().equals("City")) {
                        city = cookie.getValue();
                    } else if (cookie.getName().equals("Postcode")) {
                        postcode = cookie.getValue();
                    } else if (cookie.getName().equals("orderItems")) {
                        OrderItem[] items = APIConnection.mapper.readValue(URLDecoder.decode(cookie.getValue(), "UTF-8"), OrderItem[].class);
                        orderItems.addAll(Arrays.asList(items));
                        //out.println(URLDecoder.decode(cookie.getValue(), "UTF-8"));
                    } else if (cookie.getName().equals("CardAddress")) {
                        cardAddress = cookie.getValue();
                    } else if (cookie.getName().equals("CardCity")) {
                        cardCity = cookie.getValue();
                    } else if (cookie.getName().equals("CardPostcode")) {
                        cardPostcode = cookie.getValue();
                    } else if (cookie.getName().equals("CardCVC")) {
                        if (!cookie.getValue().equals("")) {
                            cardCvc = Integer.parseInt(cookie.getValue());
                        }
                    } else if (cookie.getName().equals("CardIssue")) {
                        if (!cookie.getValue().equals("")) {
                            cardIssueNumb = Integer.parseInt(cookie.getValue());
                        }
                    } else if (cookie.getName().equals("CardExp")) {
                        cardExpiryDate = cookie.getValue();
                    } else if (cookie.getName().equals("CardName")) {
                        cardName = cookie.getValue();
                    } else if (cookie.getName().equals("CardNum")) {
                        cardNumber = cookie.getValue();
                    } else if (cookie.getName().equals("CardType")) {
                        cardType = cookie.getValue();
                    } else if (cookie.getName().equals("FirstName")) {
                        firstName = cookie.getValue();
                    } else if (cookie.getName().equals("LastName")) {
                        lastName = cookie.getValue();
                    } else if (cookie.getName().equals("loggedIn")) {
                        if (!cookie.getValue().equals("")) {
                            loggedIn = Integer.parseInt(cookie.getValue());
                        }
                    } else if (cookie.getName().equals("PhoneNumber")) {
                        phoneNumber = cookie.getValue();
                    } else if (cookie.getName().equals("Email")) {
                        email = cookie.getValue();
                    } else if (cookie.getName().equals("PaymentType")) {
                        paymentMethod = cookie.getValue();
                    }
                }
            }
            
            address = addressLine1 + ", " + addressLine2 + ", " + city;
            cardAddress = cardAddress + ", " + cardCity;
            customerAddress = customerAddressLine1 + ", " + customerAddressLine2 + ", " + customerCity;
            
            if (loggedIn == 0) {
                customerAddress = address;
            } else {
                username = email;
            }
            
            Payment payment = new Payment(paymentId, paymentMethod, null, cardType, cardNumber, cardName, null, null, cardCvc, cardIssueNumb, cardAddress, cardPostcode);
//            out.println(APIConnection.mapper.writeValueAsString(payment));
            paymentId = APIConnection.postPaymentData(payment);

            customerId = ModelController.getInstance().createCustomer(0, firstName, lastName, customerAddress, customerPostcode, email, phoneNumber, null, username, password);
//            out.println(APIConnection.mapper.writeValueAsString(customer));
            
            
//            Order order = new Order(orderId, price, time, status, null, null, null, orderItems);
//            out.println(APIConnection.mapper.writeValueAsString(order));
            
            orderId = ModelController.getInstance().createOrder(orderId, price, time, status, customerId, paymentId, null, orderItems, address, postcode);
            
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConfirmOrderServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConfirmOrderServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
            //response.sendRedirect("OrderConfirmation.jsp");  
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
