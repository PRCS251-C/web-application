/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ndrichardson1
 */
@WebServlet(name = "PaymentServlet", urlPatterns = {"/PaymentServlet"})
public class PaymentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //getting values for the cookies
        String cardName = request.getParameter("paymentName");
        String cardNum = request.getParameter("paymentCardNo");
        String cardType = request.getParameter("paymentCardType");
        String cardCVC = request.getParameter("paymentCVC");
        String cardIssue = request.getParameter("paymentIssue");
        String cardExp = request.getParameter("paymentExp");
        String cardAddress = request.getParameter("paymentAddress");
        String cardCity = request.getParameter("paymentCity");
        String cardPostcode = request.getParameter("paymentPostcode");
        String paymentType = request.getParameter("paymentTypeP");
        //setting the cookies
        Cookie CardNameCookie = new Cookie("CardName", cardName);
        Cookie CardNumCookie = new Cookie("CardNum", cardNum);
        Cookie CardTypeCookie = new Cookie("CardType", cardType);
        Cookie CardCVCCookie = new Cookie("CardCVC", cardCVC);
        Cookie CardIssueCookie = new Cookie("CardIssue", cardIssue);
        Cookie CardExpCookie = new Cookie("CardExp", cardExp);
        Cookie CardAddressCookie = new Cookie("CardAddress", cardAddress);
        Cookie CardCityCookie = new Cookie("CardCity", cardCity);
        Cookie CardPostcodeCookie = new Cookie("CardPostcode", cardPostcode);
        Cookie PaymentTypeCookie = new Cookie("PaymentType", paymentType);
        //setting life of cookies
        CardNameCookie.setMaxAge(-1);
        CardNumCookie.setMaxAge(-1);
        CardTypeCookie.setMaxAge(-1);
        CardIssueCookie.setMaxAge(-1);
        CardCVCCookie.setMaxAge(-1);
        CardIssueCookie.setMaxAge(-1);
        CardExpCookie.setMaxAge(-1);
        CardAddressCookie.setMaxAge(-1);
        CardCityCookie.setMaxAge(-1);
        CardPostcodeCookie.setMaxAge(-1);
        PaymentTypeCookie.setMaxAge(-1);
        //adding the cookies
        response.addCookie(CardNameCookie);
        response.addCookie(CardNumCookie);
        response.addCookie(CardTypeCookie);
        response.addCookie(CardIssueCookie);
        response.addCookie(CardCVCCookie);
        response.addCookie(CardIssueCookie);
        response.addCookie(CardExpCookie);
        response.addCookie(CardAddressCookie);
        response.addCookie(CardCityCookie);
        response.addCookie(CardPostcodeCookie);
        response.addCookie(PaymentTypeCookie);
        
        
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PaymentServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PaymentServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
            response.sendRedirect("ConfirmOrder.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
