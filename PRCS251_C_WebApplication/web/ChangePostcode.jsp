<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Change postcode- Pizzariño</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style type = "text/css">
html {
    overflow-y: scroll;
}
input{
    width: 60%;
}
        </style>
        <link rel=stylesheet type="text/css" href="resources/stylesheet.css">
    </head>
    <body>
        <form name="ChangePostcodeForm" method="post" action="ChangePostcodeServlet" onkeypress="return event.keyCode != 13;">
<%           
   Cookie cookie = null;
   Cookie[] cookies = null;
   String currPostcode;
   currPostcode = "";
   cookies = request.getCookies();
   if( cookies != null ){
      for (int i = 0; i < cookies.length; i++){
         cookie = cookies[i];
         if(cookie.getName().equals("Postcode")){
             currPostcode = cookie.getValue();
             
         }
      }
  }

   
   %>            
        <table style="width:100%;height: 120px">
                    <tr>
                        <th style="width: 33%;height: 100%" align="left">
                            <div><img class="imgHeader" id="homePageLogo" src="resources/images/whitelogo2.png"></div>
                        </th>
                        
                       <th style="width: 33%;height: 100%">
                           <div class="title" style="color: white; vertical-align: top">
                           </div>
                        </th>
                        <th style="width: 33%;height: 100%" align="right">
                            <div class="title" style="color: white; padding-right:0px; display: block;">
                                <button type="button" onclick="location.href='CreateAccount.jsp';" class="genericButton2" style="height: 50px; width: 125px">Sign Up</button></br>
                                <button type="button" onclick="location.href='SignIn.jsp';" class="genericButton2" style="height:50px; width: 125px">Sign In</button>
                            </div>
                        </th>
                    </tr>
               </table>                      
                
        <table style="width: 100%">
                <td class="navBar" onclick="location.href='Menu.jsp';">Menu</td>
                <td class="navBar" onclick="location.href='Deals.jsp';">Deals</td> 
                <td class="navBar" onclick="location.href='Basket.jsp';">Checkout</td>            
        </table>
    <center>

        <p class="biggerTitle" style="color: white">Enter your updated postcode below</p>
        <p class="title" style="color: white">Your current postcode: <%=currPostcode%></p>      
        </br>
        <div style="width: 30%;">                  
                        
            <input type="text" name="postcodeInput" required class="newPostcodeInput"></br></br>
            
        </div>
                        
                <button type="button" onclick="window.history.back()" class="genericBack">Back</button>
                <button id="btnChangePostcode" class="genericForward">Change postcode</button>
    </center>    
    
    <script type="text/javascript">
        
       
    </script>
        </form>
    </body>
</html>
