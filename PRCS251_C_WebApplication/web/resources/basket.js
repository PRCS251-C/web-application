/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var itemsInBasket = 0;

function updateBasket(){
    document.getElementById("baskettable").innerHTML = "Basket: " + itemsInBasket;
}

var orderItemsCookie = getCookie("orderItems");

if (orderItemsCookie === "") {
    var orderItems = new Array();
} else {
    var orderItems = JSON.parse(orderItemsCookie);
    
    for (var i = 0; i < orderItems.length; i++) {
        itemsInBasket = itemsInBasket + parseInt(orderItems[i].ORDER_ITEM_QUANTITY);
    }

    updateBasket();
}

$(".menuAdd.pizza").click( function() {
    var id = $(this).closest(".pizzaTable").attr("id");
    var sizeId = $(this).closest(".pizzaTable").children(".menuSizeSelect").val();
    
    var orderItem = { PRODUCT_ID : id, ORDER_ITEM_SIZE : sizeId, ORDER_ITEM_QUANTITY : 1, ORDER_ID : 0, ORDER_ITEM_ID : 0};
    var contained = false;
    
    for (var i = 0; i < orderItems.length; i++) {
        if (orderItems[i].PRODUCT_ID === orderItem.PRODUCT_ID) {
            orderItems[i].ORDER_ITEM_QUANTITY = orderItems[i].ORDER_ITEM_QUANTITY + orderItem.ORDER_ITEM_QUANTITY;
            contained = true;
            break;
        }
    }
    
    if (!contained) {
        orderItems.push(orderItem);
    }
    
    setCookie("orderItems", JSON.stringify(orderItems));
    
    itemsInBasket = itemsInBasket + orderItem.ORDER_ITEM_QUANTITY;
    updateBasket();
});

$(".menuAdd.other").click( function() {
    var id = $(this).closest(".otherTable").attr("id");
    var quantity = parseInt($(this).closest(".otherTable").children(".menuQuantity").val());
    var sizeId = parseInt($(this).closest(".otherTable").children(".menuPrice").attr("id"));
    
    var orderItem = { PRODUCT_ID : id, ORDER_ITEM_SIZE : sizeId, ORDER_ITEM_QUANTITY : quantity, ORDER_ID : 0, ORDER_ITEM_ID : 0};
    var contained = false;
    
    for (var i = 0; i < orderItems.length; i++) {
        if (orderItems[i].PRODUCT_ID === orderItem.PRODUCT_ID) {
            orderItems[i].ORDER_ITEM_QUANTITY = orderItems[i].ORDER_ITEM_QUANTITY + orderItem.ORDER_ITEM_QUANTITY;
            contained = true;
            break;
        }
    }
    
    if (!contained) {
        orderItems.push(orderItem);
    }
    
    setCookie("orderItems", JSON.stringify(orderItems));
    
    itemsInBasket = itemsInBasket + orderItem.ORDER_ITEM_QUANTITY;
    updateBasket();
});

function setCookie(cookieName, jsonObject) {
    document.cookie = cookieName + "=" + encodeURIComponent(jsonObject) + ";" + "expires=-1;path=/";
}

function getCookie(cookieName) {
    var name = cookieName + "=";
    var cookieArray = document.cookie.split(";");
    
    for (var i = 0; i < cookieArray.length; i++) {
        var cookie = cookieArray[i];
        
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1);
        }
        
        if (cookie.indexOf(name) === 0) {
            return decodeURIComponent(cookie.substring(name.length, cookie.length));
        }
    }
    
    return "";
}